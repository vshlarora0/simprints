
Challenge 1

Approach - For counting the alphanumeric characters, I have included the count for uppercase letters (A-Z) with the corresponding lower case character (a-z) and ignored characters other than digits(0-9) and alphabets([A-Za-z]). 

Challenge 2

Approach - The output from the input matrix is the sum of the diagonal elements. When there are even no of rows then there is no overlapping cell. For odd no of rows, we have to count the overlapping value only once.

Challenge 3 

Approach - Used a comparator to sort the ascii sum values of all characters in the values.

