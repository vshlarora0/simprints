package com.company;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AsciiValues {

    private static final List<String> VALUES;

    static {
        VALUES = new ArrayList<>();
        VALUES.add("Relentless Commitment to Impact");
        VALUES.add("Robust as Fudge");
        VALUES.add("Be Surprisingly Bold");
        VALUES.add("Get Back Up");
        VALUES.add("Make It Happen");
        VALUES.add("Don't Be a Jerk");
        VALUES.add("Confront the Grey");
        VALUES.add("Laugh Together, Grow Together");
    }

    public static void main(String[] args) {
        List<SimprintsValue> simprintsValues = new ArrayList<>(VALUES.size());

        for (String valueText : VALUES) {
            int asciiValue = getAsciiSumForText(valueText);
            simprintsValues.add(new SimprintsValue(valueText, asciiValue));
        }

        // sort values in decreasing order of ascii values
        simprintsValues.sort(ASCII_VALUE_DESC);

        for (SimprintsValue simprintsValue : simprintsValues) {
            System.out.println(simprintsValue);
        }
    }

    /**
     * Comparator to sort Simprint Value in decreasing order of ASCII sum.
     */
    static Comparator<SimprintsValue> ASCII_VALUE_DESC = (o1, o2) -> {
        if (o1.getAsciiSum() == o2.getAsciiSum()) return 0;
        return o1.getAsciiSum() > o2.getAsciiSum() ? -1 : 1;
    };

    /**
     * @param text Simprint Value in text form
     * @return ASCII sum of all the characters in the value
     */
    static int getAsciiSumForText(String text) {
        int asciiSum = 0;
        for (char c : text.toCharArray()) {
            asciiSum += c;
        }

        return asciiSum;
    }

    /**
     * A class to encapsulate Text and ASCII sum for a Simprint Value
     */
    static class SimprintsValue {
        private final String text;
        private final int asciiSum;

        SimprintsValue(String text, int asciiSum) {
            this.text = text;
            this.asciiSum = asciiSum;
        }

        int getAsciiSum() {
            return asciiSum;
        }

        @Override
        public String toString() {
            return "SimprintsValue{" +
                    "text='" + text + '\'' +
                    ", asciiSum=" + asciiSum +
                    '}';
        }
    }
}
