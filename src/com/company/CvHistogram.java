package com.company;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CvHistogram {

    private static final char[] DIGIT_LIST = "0123456789".toCharArray();
    private static final char[] ALPHABET_LIST = "abcdefghijklmnopqrstuvwxyz".toCharArray();

    public static void main(String[] args) {

        if (args.length < 1) {
            System.out.println("CV File path is not mentioned in argument.");
            terminate();
        }

        String cvFilePath = args[0];

        FileReader fr = null;
        try {
            fr = new FileReader(cvFilePath);
        } catch (FileNotFoundException e) {
            System.out.println("Unable to locate cv file at the provided path " + cvFilePath);
            terminate();
        }

        final int[] digitsCountArray = new int[DIGIT_LIST.length];
        final int[] alphabetCountArray = new int[ALPHABET_LIST.length];

        try {
            int i;
            while ((i = fr.read()) != -1) {
                char character = (char) i;
                if (isDigit(character)) {
                    digitsCountArray[getIndexForDigit(character)]++;
                } else if (isAlphabet(character)) {
                    alphabetCountArray[getIndexForAlphabet(character)]++;
                }
            }
        } catch (IOException e) {
            System.out.println("Error reading cv file " + cvFilePath);
            terminate();
        }

        // print digits bar
        for (char digit : DIGIT_LIST) {
            printBar(digit, digitsCountArray[getIndexForDigit(digit)]);
        }

        // print alphabet bar
        for (char alphabet : ALPHABET_LIST) {
            printBar(alphabet, alphabetCountArray[getIndexForAlphabet(alphabet)]);
        }
    }

    private static void printBar(char character, int count) {
        System.out.println();
        System.out.print(character + ": ");
        for (int i = 0; i < count; i++) {
            System.out.print('-');
        }
    }

    private static boolean isAlphabet(char character) {
        return character >= 65 && character <= 90 || character >= 97 && character <= 122;
    }

    private static boolean isDigit(char character) {
        return character >= 48 && character <= 57;
    }

    private static int getIndexForDigit(char character) {
        return character - 48;
    }

    private static int getIndexForAlphabet(char character) {
        if (character >= 65 && character <= 90) return character - 65;
        if (character >= 97 && character <= 122) return character - 97;
        return -1;
    }

    private static void terminate() {
        System.out.println("Terminating Program.");
        System.exit(-1);
    }
}
