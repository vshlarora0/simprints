package com.company;

import java.util.Scanner;

public class TrainingSet {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int rowCount = sc.nextInt();
        int matrix[][] = new int[rowCount][rowCount];

        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < rowCount; j++) {
                matrix[i][j] = sc.nextInt();
            }
        }

        int diagonalSum = 0;

        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < rowCount; j++) {
                if (i == j) {
                    diagonalSum += matrix[i][j] + matrix[j][i];
                }
            }
        }

        if (rowCount % 2 != 0) {
            int overlappingDiagonalElementIndex = rowCount / 2;
            diagonalSum -= matrix[overlappingDiagonalElementIndex][overlappingDiagonalElementIndex];
        }

        System.out.println("Output : " + diagonalSum);
    }
}
